#include <stdint.h>
#include <stdio.h>

#define BYTETUPLE_MAX UINT32_MAX
typedef uint32_t bytetuple;

int main (void)
{
   FILE * b;
   FILE * f;
   bytetuple datum[1];
   if (f = fopen ("RandomOrg.pts", "w")) {
	    if (b = fopen ("RandomOrg.bin", "r")) {
         while ((fread(datum, sizeof(bytetuple), 1, b)) == 1) {
            fprintf(f, "%a\n", (double)(*datum)/BYTETUPLE_MAX);
         }
         fclose (f);
      }
      fclose (b);
   }
   return 0;
}
